﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.AI;

public class Spawner : MonoBehaviour {
	public Transform start;
	public Transform finish;
	public Alien prefabAlien;
	public Canvas prefabAlienUI;
	public ConfigPanel config;
	public Director director;
	public GameObject camara;
    public float ContSpawn;
    public List<Alien> Aliens;
    public static float suma =0;
    public static float suma2;
    float promedio = 0;

    public void ResetAlien(Alien alien)
	{
        Aliens.Add(alien);

        //sumo la cantidad de aliens vivos
        Director.aliensAlive++;
        //seteo la velocidad del alien
        prefabAlien.alienAgent.speed = ConfigPanel.speed;

        alien.ui.transform.position = alien.transform.position = start.transform.position;
        alien.transform.rotation = start.transform.rotation;

        //le pongo un nombre distinto a cada alien para identificarlos mejor
        prefabAlien.name = "Alien" + Director.aliensAlive.ToString();

        alien.Reset();
	}

	public Alien CreateAlien()
	{
		var alien = Instantiate(prefabAlien, this.transform);
		alien.ui = Instantiate(prefabAlienUI, this.transform);
		ResetAlien(alien);

		return alien;
	}

	public void Start () 
	{
        //inicializo variables en 0 
        suma = 0;
        promedio = 0;
    }

    public void AverageAngle()
    {
        //utilizo un for para identificar a cada alien en una lista de aliens para acceder a cada angulo y asi poder sacar el promedio
        for (int i = 0; i < Director.aliensAlive; i++)
        {
            if (Director.aliensAlive > 1)
                suma += Aliens[i].angulo + Aliens[i++].angulo;
        }
        if (Director.aliensAlive == 1)
        {
            promedio = Aliens[0].angulo;
        }
        else if (Director.aliensAlive > 1)
        {
            //hago una distincion entre si el numero de aliens vivos es par o impar ya que cuando el numero es impar se contaba el primer angulo de mas, y de esta manera consigo suplirlo
            if ((Director.aliensAlive % 2) == 0 || Director.aliensAlive == 1)
            {
                promedio = suma / Director.aliensAlive;
            }
            else
            {
                promedio = (suma - Aliens[0].angulo) / Director.aliensAlive;
            }
        }
        Director.avgAngle = promedio;
        Debug.Log("Suma es igual a " + suma);
        Debug.Log("El promedio es" + promedio);
    }

    void Update()
    {
        SpawnRateAndSpeed();
        ShowAverageAngle();
    }

    public void ShowAverageAngle()
    {
        if (Director.aliensAlive < 1)
        {
            Director.avgAngle = 0;
        }
        //hacer click derecho para actualizar el angulo del panel
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            AverageAngle();
            suma = 0;
        }
    }

    public void SpawnRateAndSpeed()
    {
        //utilizo un contador que se encarga del rate de spawneo de aliens, soy consciente de que existen mejores formas de hacerlo
        ContSpawn += Time.deltaTime;
        //tambien vuelvo a setear la velocidad global de los aliens
        prefabAlien.alienAgent.speed = ConfigPanel.speed;
        //utilizo un if para cada condicion del slider
        if (ConfigPanel.rate == 1)
        {
            if (ContSpawn >= 1)
            {

                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 2)
        {
            if (ContSpawn >= 2)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;


                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 3)
        {
            if (ContSpawn >= 3)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 4)
        {
            if (ContSpawn >= 4)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 5)
        {
            if (ContSpawn >= 5)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 6)
        {
            if (ContSpawn >= 6)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 7)
        {
            if (ContSpawn >= 7)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 8)
        {
            if (ContSpawn >= 8)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 9)
        {
            if (ContSpawn >= 9)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }

        if (ConfigPanel.rate == 10)
        {
            if (ContSpawn >= 10)
            {
                var alien = CreateAlien();
                alien.camara = camara;
                alien.target = finish.transform;

                alien.alienAgent.enabled = false;

                alien.speed = ConfigPanel.speed;
                ContSpawn = 0;
            }
        }
    }
}


