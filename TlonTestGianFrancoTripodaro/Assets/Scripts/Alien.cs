﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.AI;

public class Alien : MonoBehaviour {
    public Canvas ui;
    public Text txtAlien;
    public Transform target;
    public float speed;
    public NavMeshAgent alienAgent;
    public GameObject camara;
    public int CocoonCont;
    public float angulo;


    void Start()
    {
        alienAgent = GetComponent<NavMeshAgent>();
        txtAlien = ui.GetComponentInChildren<Text>();
        txtAlien.text = "0";
        Reset();
    }

    public void Reset()
    {
        GetComponent<Animator>().Play("Grounded");
        GetComponent<Animator>().SetFloat("Forward", 1f);
    }

    void Update()
    {
        AlienAngle();
        AlienMov();
        AlienUITransform();
    }

    public void AlienUITransform()
    {
        if (ui != null)
        {
            ui.transform.position = transform.position /*+ new Vector3(0, 2, 0)*/;
        }
    }

    public void AlienMov()
    {
        if (target != null)
        {
            var delta = target.position - transform.position;
            delta.y = 0f;
            var deltaLen = delta.magnitude;

            alienAgent.speed = ConfigPanel.speed;
            //cuando el alien no esta a 5f de distancia del target se activa y comienza a ir hacia el target.
            if (deltaLen <= 5f)
            {
                GetComponent<Animator>().SetFloat("Forward", 0f);
                alienAgent.enabled = false;

            }
            else
            {
                var direction = delta / deltaLen;

                alienAgent.enabled = true;
                alienAgent.destination = target.position;

            }
        }
    }

    public void AlienAngle()
    {
        //utilizo el target del alien como referencia para marcar el angulo
        angulo = CalcAngle.anguloEntrePuntos(target.position, transform.position);

        Debug.Log(angulo);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cocoon"))
        {
            CocoonCont++;
            txtAlien.text = CocoonCont.ToString();
        }

        if (other.gameObject.CompareTag("End"))
        {
            Director.aliensAlive--;
            Destroy(this.gameObject);
            Destroy(this.txtAlien);
            
        }

    }
}
