﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ConfigPanel : MonoBehaviour {

	public Slider sliderRate;
	public Slider sliderSpeed;
	public Text txtRate;
	public Text txtSpeed;
	public Text txtInfo;

    public static int rate { get; private set; }
	public static float speed { get; private set; }
	
	void Update ()
	{

		VarPanel();

    }

	//utilizo metodos para las variables con el fin de que el codigo sea mas facil de leer y modificar
	public void VarPanel()
    {
		rate = Mathf.RoundToInt(sliderRate.value);
		txtRate.text = rate.ToString("0.");
		speed = sliderSpeed.value;
		txtSpeed.text = speed.ToString("0.");
	}

}
