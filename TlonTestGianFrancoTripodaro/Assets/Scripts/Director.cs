﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Director : MonoBehaviour {
	public Spawner spawner;
	public ConfigPanel config;

	public static int aliensAlive = 0;
	public static int hitsPerSecond = 0;
	public static float avgAngle = 0f;

	public static float contHitPerSecond;

	public Transform alien;
	public Transform end;
	void Start ()
	{
		//se inicializan en 0 las variables
		aliensAlive = 0;
		hitsPerSecond = 0;
		avgAngle = 0;
	}

	void Update ()
	{
		VarDirector();
	}

	public void VarDirector()
    {
		//cuenta un segundo para calcular el hit por segundo de los marcianos con los Cocoons
		contHitPerSecond += Time.deltaTime;

		//Update stats
		config.txtInfo.text =
			$"{aliensAlive} aliens alive"
			+ $"\n{hitsPerSecond} cocoons hits p/sec"
			+ $"\n{avgAngle:F2}° average angle"
			;
	}
}
