﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cocoon : MonoBehaviour {

	//para detectar las coliciones por segundo de aliens contra Cocoons utilizo el timer llamado contHitPerSecond
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Alien"))
		{
			if (Director.contHitPerSecond > 1)
			{
				Director.hitsPerSecond++;
				Director.contHitPerSecond = 0;
			}
		}
	}
}
