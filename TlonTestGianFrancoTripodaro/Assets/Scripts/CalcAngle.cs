﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcAngle : MonoBehaviour {

	public static float anguloEntrePuntos(Vector3 p1, Vector3 p2)
	{
		//utilizo este script para inicializar las variables de calculo de vectores y retornar la variable que necesito utilizar
		Vector3 vector = p1 - p2;
		vector.Normalize();
		float angle = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
		return angle;
	}
}
